import { ListenOptions } from "node:net";
import *  as http from "node:http";
import { SourceFile } from "ts-morph";
import * as _ from "lodash";
import * as formidable from "formidable";
import cookieParser = require("cookie-parser");

export interface ApiifyRoute {
  path: string;
  func: Function;
  args: Function[];
}

export class ApiifyRouteFile {
  __source__: SourceFile;
  file: string;
  path: string;
  name?: string;
  mtimeMs: number;
  // parametersType: Array<{
  //   parameter: ParameterDeclaration;
  //   typeText: string;
  // }>
  // args: Array<(req: http.IncomingMessage) => {}>
  defaultRoute?: {
    parameters: Parameter[]
  };
  methods: Array<{
    name: string,
    func: Function,
    parameters: Parameter[],
    returnType: string,
  }>
  // fncs: any[];
  constructor({
    __source__,
    file,
    path,
    name,
    mtimeMs,
    defaultRoute,
    methods
  }: {
    __source__: SourceFile,
    file: string,
    path: string,
    name?: string,
    mtimeMs: number;
    defaultRoute?: {
      parameters: Parameter[]
    };
    methods: Array<{
      name: string,
      func: Function,
      parameters: Parameter[],
      returnType: string
    }>
  }) {
    this.__source__ = __source__;
    this.file = file;
    this.path = path;
    this.name = name;
    this.mtimeMs = mtimeMs;
    this.defaultRoute = defaultRoute;
    this.methods = methods;
  }

  toString() {
    return JSON.stringify({
      path: this.path,
      // name: this.name,
      // mtimeMs: this.mtimeMs,
      // defaultRoute: this.defaultRoute,
      // methods: this.methods,
    })
  }

  toJSON() {
    return this.toString();
  }
}

export interface RouteContent {
  path: string,
  params?: any;
  methods: string[];
  func: Function;
  args: Array<Handler>;
  parameters: Parameter[];
}

export interface Options {
  listen?: ListenOptions;
  rootDir?: string;
  routeDir?: string;
  argsFilePath?: string;
  apiifyWorkPath?: string;
  server?: {
    /**
     * can set 0 to disable and -1 to create amount cpu.length
     */
    worker?: number;
    /**
     * this value will override to server.worker if 'NODE_ENV' is 'production'
     */
    productionWorker?: number;
  }
  request?: {
    /** 
     * default is '100KB'
     * can set '0' to unlimit
     * https://www.npmjs.com/package/bytes 
     * */
    limitSize?: string;
    /** 
     * default is '10MB'
     * can set '0' to unlimit
     * https://www.npmjs.com/package/bytes 
     * */
    uploadLimitSize?: string;
  },
  /**
   * @see https://www.npmjs.com/package/formidable#options
   */
  formidable?: formidable.Options,
  cookieParser?: {
    options?: cookieParser.CookieParseOptions,
    secret?: string | string[]
  },
  basePath?: string;
  https?: {
    enable: boolean;
    keyFile?: string;
    certFile?: string;
    caFile?: string;
    redirectPort?: number;
  }
}

export interface InnerOptions {
  main: string;
  listen: ListenOptions;
  rootDir: string;
  routeDir?: string;
  argsFilePath?: string;
  apiifyWorkPath?: string;
  server?: {
    worker?: number;
  }
  request: {
    limitSize?: number;
    uploadLimitSize?: number;
  },
  formidable?: formidable.Options,
  cookieParser?: {
    options?: cookieParser.CookieParseOptions,
    secret?: string | string[]
  },
  basePath: string;
  https?: {
    enable?: boolean;
    keyFile?: string;
    certFile?: string;
    caFile?: string;
    redirectPort?: number;
  }
}

export class Parameter {
  name: string;
  rawType?: string;
  type: string;
  // typeRawText: string;
  // fullText: string;
  isOptional?: boolean;
  isArray: boolean;
  extends: Array<{
    text: string;
  }> = [];
  _method?: any;

  constructor({
    name,
    type,
    isArray,
    isOptional,
    rawType,
    extendsList,
    method
  }: {
    name: string,
    type: string,
    isArray: boolean,
    isOptional: boolean,
    rawType?: string,
    extendsList?: Array<{ text: string }>,
    method?: any
  }) {
    this.name = name;
    this.type = type;
    this.isArray = isArray;
    this.isOptional = isOptional;
    this.rawType = rawType;
    this.extends = extendsList || [];
    this._method = method;
  }

  toJSON() {
    const { _method, ...rest } = this;
    return rest;
  }
}

export interface MatchList {
  matcher: (p: Parameter) => boolean | Promise<boolean> | Function;
  handler?: Handler
}

export enum RequestMethods {
  ALL = 'ALL',
  CONNECT = 'CONNECT',
  DELETE = 'DELETE',
  GET = 'GET',
  HEAD = 'HEAD',
  OPTIONS = 'OPTIONS',
  POST = 'POST',
  PUT = 'PUT',
  TRACE = 'TRACE',
}

export type Handler = (req: http.IncomingMessage, res: http.ServerResponse, p: Parameter, params?: any) => any;

export default {}
