import * as http from "http";
import { Options } from "./types";
import { debug, Runtime, runtime, stackTrace } from "./utils";
import * as fse from "fs-extra";
import * as path from "node:path";
import loadConfig from "./utils/load-config";
import * as url from "node:url";
import * as _ from "lodash";
import { ApiifyServer, optionsTransfer } from "./apiify-server";
import * as mime from "mime-types";
import HttpStatusCode from "./utils/http-status";
import { Readable, Stream } from "node:stream";
import formidable = require("formidable");
import dayjs = require("dayjs");
import * as zlib from "zlib";
import merge from "cat-config/dist/utils/deepmerge";
import * as CookieParser from "cookie-parser";
import { CookieSerializeOptions } from "cookie";

export class ApiifyError {
  code: number;
  message: string;
  properties = {};
  // for OpenAPI
  _description?: string = '';

  constructor(messageOrOptions: string | { code?: number, message?: string }, properties = {}) {
    if (typeof messageOrOptions === 'string') {
      this.code = 500;
      this.message = messageOrOptions;
    } else {
      const { code, message } = messageOrOptions;
      this.code = code || 500;
      // @ts-ignore
      this.message = message || HttpStatusCode[code] || 'Unassigned';
    }

    this.properties = properties;
  }
}

export class ValidationErrorClass extends ApiifyError {
  constructor(msg: string) {
    super({ code: 400, message: msg });
  }
}

export namespace Apiify {
  export class RequestData {
    [field: string]: any;
  }

  export class RequestBody extends RequestData { }

  export class RequestQuery extends RequestData { }

  export function Error(code = 500, message?: string, properties = {}): ApiifyError {
    if (typeof message !== 'string')
      message = JSON.stringify(message);

    return new ApiifyError({ code, message });
  }

  export function ValidationError(msg: string) {
    return new ValidationErrorClass(msg);
  }

  export async function create(optionsOrRootDir: Options | string = {}) {
    let op: Options;

    if (typeof optionsOrRootDir === 'string') {
      if (path.isAbsolute(optionsOrRootDir)) {
        op = await loadConfig(optionsOrRootDir);
      } else {
        const rootDir = runtime == Runtime.NODEJS ?
          path.resolve(path.dirname(stackTrace()[6]), '../', optionsOrRootDir) :
          path.resolve(path.dirname(stackTrace()[6]), optionsOrRootDir)

        op = await loadConfig(rootDir);
      }
    } else {
      op = optionsOrRootDir
    }
    const options = optionsTransfer(op);

    const apiify = new ApiifyServer(options);

    await Promise.all([
      options.argsFilePath && await apiify.loadArgumentMatchList(options.argsFilePath),
      (options.routeDir && await fse.pathExists(options.routeDir)) && await apiify.loadRouteDir(options.routeDir, options)
    ]);

    return apiify
  }

  export function useQuery<T = any>(req: http.IncomingMessage): T {
    if (!_.get(req, 'query')) {
      const query = {};
      let qr = url.parse(_.get(req, 'url', '/'), true).query

      if (_.has(qr, '_')) {
        qr = JSON.parse(_.get(qr, '_', '{}') as string);
      }

      for (const k of Object.keys(qr)) {
        if (/\[\]$/.test(k)) {
          _.set(query, k.replace('[]', ''), Array.isArray(qr[k]) ? qr[k] : [qr[k]]);
        } else {
          _.set(query, k, qr[k]);
        }
      }


      _.set(req, 'query', query);
    }

    return _.get(req, 'query') as T;
  }

  function contentEncoding(req: http.IncomingMessage) {
    const encoding = (req.headers['content-encoding'] || 'identity').toLowerCase();
    let st;
    switch (encoding) {
      case 'deflate':
        debug('content-encoding "%s"', encoding);
        st = req.pipe(zlib.createInflate());
        break;
      case 'gzip':
        debug('content-encoding "%s"', encoding);
        st = req.pipe(zlib.createGunzip());
        break;
      case 'identity':
        st = req;
        break;
      default:
        throw Apiify.Error(415, 'unsupported content encoding "' + encoding + '"', {
          encoding: encoding,
          type: 'encoding.unsupported'
        })
    }
    return st;
  }

  function contentTypeParser<T = any>(req: http.IncomingMessage): Promise<T> {
    return new Promise(async (resolve, reject) => {
      const st = contentEncoding(req) as http.IncomingMessage;
      const app: ApiifyServer =
        // @ts-ignore
        <ApiifyServer>_.get(req, 'app');

      const contentType = req.headers['content-type'];

      if (contentType && /^multipart\/form\-data;/g.test(contentType)) {
        const limit = app.options.request.uploadLimitSize;
        let length = 0;
        req.on('data', chunk => {
          length += chunk.length;
          if (limit && length > limit) {
            reject(Apiify.Error(413));
            setTimeout(() => st.destroy());
          }
        });

        try {
          const formidable = await import('formidable');
          const form = new formidable.IncomingForm(app.options.formidable);


          let body: any = {};

          const files: formidable.Part[] = [];
          const p: Promise<any>[] = [];

          // form.onPart = (part) => {
          //   // filter file
          //   if (_.has(part, 'headers.content-type')) {
          //     // @ts-ignore
          //     let arr: formidable.Part[] = _.get<formidable.Part[]>(body, part.name, []);
          //     // @ts-ignore
          //     arr.push(part);
          //     let s: ReadableStream<any> = new ReadableStream();

          //     // // @ts-ignore
          //     // arr = arr.compose(s)
          //     // @ts-ignore
          //     _.set(body, part.name, arr);
          //     arr[0].on('end', () => {
          //       console.log('end');

          //     })
          //     // arr[0].on('data', c => {
          //     //   console.log(c);

          //     // })

          //   } else {
          //     p.push(
          //       new Promise(async (resolve, reject) => {
          //         let value = '';
          //         part.on('data', data => {
          //           value += data.toString();
          //         })
          //         part.on('end', () => {
          //           // @ts-ignore
          //           let arr = _.get<formidable.Part[]>(body, part.name, []);
          //           // @ts-ignore
          //           arr.push(value);
          //           // @ts-ignore
          //           _.set(body, part.name, arr);
          //           resolve(value);
          //         });
          //       })
          //     );

          //     return part
          //   }
          // }

          form.parse(st, async (err, fields, files) => {
            if (err) return reject(err);
            await Promise.all(p);
            body = { ...body, ...fields, ...files }

            // for (const k of Object.keys(body)) {
            //   if (body[k].length == 1)
            //     body[k] = body[k][0];
            // }
            resolve(body as T);
          });
        } catch (e) {
          return reject(e);
        }
      } else {
        let rawBody = '';
        const limit = app.options.request.limitSize;
        let length = 0;
        st.on('data', chunk => {
          length += chunk.length;
          if (limit && length > limit) {
            reject(Apiify.Error(413));
            setTimeout(() => st.destroy());
          }
          rawBody += chunk.toString();
        });

        st.on('end', () => {
          if (contentType === 'application/x-www-form-urlencoded') {
            const query = {};
            const qr = url.parse('/?' + rawBody, true).query
            for (const k of Object.keys(qr)) {
              _.set(query, k, qr[k]);
            }

            resolve(query as T);
          } else if (contentType === 'application/json') {
            resolve(rawBody ? JSON.parse(rawBody) : {});
          } else if (contentType === undefined || /^text\//g.test(contentType)) {
            resolve(rawBody as T);
          } else {
            reject(Apiify.Error(415, 'unsupported content encoding "' + contentType + '"'));
          }
        });
      }
    });
  }

  export async function useBody<T = any>(req: http.IncomingMessage): Promise<T> {
    if (!<T>_.get(req, '__context__.body_promise')) {
      _.set(req, '__context__.body_promise', new Promise(async (resolve, reject) => {
        contentTypeParser<T>(req)
          .then(resolve)
          .catch(reject);
      }))
    }

    return _.get(req, '__context__.body_promise') as T;
  }

  export async function useBodyRaw<T = any>(req: http.IncomingMessage) {
    await useBody(req);
    return _.get(req, '__context__.body_promise') as T;
  }

  export async function useRequestJson<T = any>(req: http.IncomingMessage, schema?: any): Promise<T> {
    if (!<T>_.get(req, '__context__.json_promise')) {
      const p = new Promise(async (resolve, reject) => {

        let body = await Apiify.useBody(req);
        const contentType = req.headers['content-type'] || 'text/plain';
        if (/^text\//g.test(contentType)) {
          const { parse } = await import('yaml');
          try {
            body = parse(body)
          } catch (e) { }
        }

        const data: T = await merge(body || {}, await Apiify.useQuery(req) || {});

        resolve(data);
      });

      _.set(req, '__context__.json_promise', p);
    }

    if (schema) {
      const d = await _.get(req, '__context__.data_promise') as T;
      let r = {};

      for (const key of Object.keys(schema)) {
        const s = schema[key];
        let v = _.get(d, key);
        if (typeof v === 'undefined' && s.require) {
          throw Apiify.ValidationError(`require '${key}'`)
        }

        if (typeof v !== 'undefined')
          if (s.type === Number) {
            v = parseFloat(v);
          }

        _.set(r, key, typeof v === 'undefined' ? s.default : v);
      }
      return r as T;
    } else {
      return _.get(req, '__context__.json_promise') as T;
    }
  }

  export function useConfig(): Options {
    return loadConfig();
  }

  /**
   * This method is based on formidable.Part
   */
  export interface FileStream extends Readable {
    name: string;
    originalFilename: string;
    mimetype: string;
  }

  export async function sendFile(req: http.IncomingMessage, filePath: string, { compress } = { compress: false }) {

    // @ts-ignore
    const res = req['res'] as http.ServerResponse;

    const stat = await fse.stat(filePath)
    const lastModified = stat.mtime.toUTCString();

    if (req.headers['if-modified-since'] === lastModified) {
      res.writeHead(304, 'Not Modified');
      res.end();
    } else {
      // res.writeHead(200, {
      //   'Content-Type': mime.lookup(filePath) || 'application/octet-stream',
      //   'Content-Length': stat.size,
      //   'Last-Modified': lastModified
      // });

      res.setHeader('Content-Type', mime.lookup(filePath) || 'application/octet-stream');
      res.setHeader('Last-Modified', lastModified);

      const stream = fse.createReadStream(filePath);

      let s: http.ServerResponse<http.IncomingMessage>;
      if (compress) {
        res.setHeader('Content-Encoding', 'br');
        s = stream.pipe(zlib.createBrotliCompress()).pipe(res);
      } else {
        res.setHeader('Content-Length', stat.size);
        s = stream.pipe(res);
      }
      await new Promise(r => {
        s.on('end', r)
      })
    }
  }

  interface FileOptions {
    compress?: boolean;
  }

  /** for response file
   * example:
   * new Apiify.File('file.txt')
   */
  export class File {
    absolutePath: string;
    options: FileOptions = {};
    filepath?: string;
    mimetype?: string;

    constructor(fielPath: string, options: FileOptions = {}) {
      this.options = options;
      if (path.isAbsolute(fielPath))
        this.absolutePath = fielPath;
      else
        this.absolutePath = path.resolve(path.dirname(stackTrace()[1]), fielPath)

      this.filepath = this.absolutePath;
    }
  }

  /**
   * This method is based on Dayjs
   */
  export interface DayJs extends dayjs.Dayjs { }

  export function useCookies(req: http.IncomingMessage) {
    // @ts-ignore
    if (!req.cookies) {
      // @ts-ignore
      const res = req[res];
      // @ts-ignore
      CookieParser()(req, res, () => { });
    }

    // @ts-ignore
    const c = new Cookies(req.cookies);
    // @ts-ignore
    req.__cookies = c;
    return c;
  }

  export function useSignedCookies(req: http.IncomingMessage): Record<string, string> {
    // @ts-ignore
    if (!req.signedCookies) {
      // @ts-ignore
      const res = req[res];
      // @ts-ignore
      CookieParser()(req, res, () => { });
    }

    // @ts-ignore
    return req.signedCookies;
  }

  export class Cookies<T = any> {
    [T: string]: any;
    private __options__: any = {};

    constructor(recoeds: any) {
      for (const key of Object.keys(recoeds)) {
        this[key] = recoeds[key];
      }
    }

    set(name: string, value: any, options?: CookieSerializeOptions): void {
      let v = (typeof value === 'string') ? value : JSON.stringify(value);
      this[name] = v;
      this.__options__[name] = options;
    }
  }

  export interface SignedCookies extends Record<string, string> { }

  export interface HTTPBasicAuth {
    username?: string;
    password?: string;
  }
}

export default Apiify
