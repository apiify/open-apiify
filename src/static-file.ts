import dayjs from "dayjs";
import * as fse from "fs-extra";
import * as http from "http";
import * as path from "node:path";
import Apiify from ".";
import { stackTrace } from "./utils";
import prettyBytes from "./utils/pretty-bytes";
import { ApiifyServer } from "./apiify-server";

export interface StaticOptions {
  showDirectoryListings: boolean;
  autoindex?: boolean;
  cacheMaxAge?: number;
}

export default function (app: ApiifyServer, dirPath: string, options: StaticOptions = {} as StaticOptions) {
  function requestListener(req: http.IncomingMessage, res: http.ServerResponse) {
    const p = (req.url || '/').replace(/^\/public\//gmi, '')
    const filePath = path.join(dirPath, p);

    if (options.cacheMaxAge === undefined || options.cacheMaxAge >= 0) {
      res.setHeader('Cache-Control', `immutable, max-age=${options.cacheMaxAge || 604800}`)
      // res.setHeader('Age', 100)
    }
    if (fse.existsSync(filePath)) {
      if (fse.lstatSync(filePath).isDirectory()) {
        if (options.autoindex && fse.existsSync(path.join(filePath, 'index.html'))) {
          return new Apiify.File(path.join(filePath, 'index.html'));
        } else if (options.showDirectoryListings) {
          res.setHeader('Content-Type', 'text/html')
          let html = `<h1 style="margin-block-end: 0.67em; margin-block-start: 0.67em;">Index of ${req.url || ''}</h1>\n`;
          const files = fse.readdirSync(filePath);
          html += '<table>\n';
          html += `<tr><td></td><td></td><td><a href="${req.url}/../">../</a><br></div></td></tr>\n`
          files.map(f => {
            const stat = fse.statSync(path.join(filePath, f));

            html += '<tr>\n'
            html += `<td>${dayjs(stat.mtimeMs).format('D-MMM-YYYY H:mm')}</td>\n`
            html += `<td>${prettyBytes(stat.size)}</td>\n`
            html += `<td class="name"><a href="${req.url || ''}/${f}">${f}</a></td>\n`
            html += '</tr>\n'
          })
          html += '<table>\n';
          html += `<style>
          td { padding-left: 10px; padding-right: 10px; }
          .name { padding-bottom: 5px; }
          </style>\n`
          return html;
        }
      } else {
        return new Apiify.File(filePath)
      }
    }

    return null;
  }

  app.get('/public', requestListener);
  app.get('/public/**', requestListener);
}