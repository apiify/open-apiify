#!/usr/bin/env node

import * as commander from "commander";
// import * as pkg from "../package.json";
import { Command } from "commander";
import * as path from "node:path";
import * as fse from "fs-extra";
import * as _ from "lodash";
import * as JSON5 from "json5";
import { execSync } from "node:child_process";
import { ApiifyRouteFile } from "./types";
import { listFiles, debug } from "./utils";
import RouteFile from "./utils/apiify-route-file"
import * as shelljs from "shelljs";
import { consola } from "consola";

const pwd = process.cwd().replace(/\\/gm, '/');
function init() {
  consola.start('initial project...');
  const templateDir = path.resolve(__dirname, '../template');
  fse.copySync(templateDir, pwd);
  execSync(`yarn add open-apiify`, { stdio: 'inherit' });
  execSync(`yarn add -D @types/node typescript nodemon yaml`, { stdio: 'inherit' });
  consola.success("Project initial");
}

async function build() {
  consola.start('build...');
  shelljs.cd(process.cwd().replace(/\\/gm, '/'));
  execSync('yarn tsc', { stdio: 'inherit' });

  const routeDir = path.resolve(pwd, 'src/routes');
  let files = await listFiles(routeDir);
  const apiifyRouteFiles: ApiifyRouteFile[] =
    (await Promise.all(
      files.map(async (file) => {
        return RouteFile.create(routeDir, file, true);
      })
    ))
      .sort((a, b) => {
        const x = a.path.replace(/[^\/]/gmi, '').length - b.path.replace(/[^\/]/gmi, '').length;
        const y = a.path.length - b.path.length;
        return x !== 0 ? x : y;
      });

  shelljs.mkdir('-p', path.resolve(pwd, '.apiify'));
  let fileDescriptor = await fse.open(path.resolve(pwd, 'dist/routes.json'), 'w+');
  await fse.writeFile(fileDescriptor, JSON.stringify(apiifyRouteFiles.map(({ __source__, ...rest }) => {
    return {
      ...rest,
      methods: rest.methods.map(m => {
        return {
          name: m.name,
          parameters: m.parameters
        }
      })
    }
  }), null, 2))
  fse.close(fileDescriptor);
  consola.success("build success");
}

const program = new commander.Command();

async function getOutputDir() {
  const pwd = process.cwd().replace(/\\/gm, '/');
  const json = JSON5.parse(await fse.readFile(path.resolve(pwd, 'tsconfig.json'), 'utf-8'));
  return _.get(json, 'compilerOptions.outDir');
}

program
  .command('init')
  .description('init project')
  .action((options) => {
    init();
  });

// program
//   .version(pkg.version);
program
  .command('build')
  .action(async () => {
    build();
  });

program
  .command('dev', { isDefault: true })
  .description('launch web server in dev mode')
  .option('-V, --version')
  .option('--inspect')
  .action(async (options) => {
    if (options.version) {
      let pkg: { version: string } = JSON.parse(fse.readFileSync(path.resolve(__dirname, '../', 'package.json'), 'utf-8'));
      console.log(pkg.version);
      return;
    }

    debug('command: dev');
    if (fse.readdirSync(pwd).length <= 1)
      init();

    const fileExecPath = (fse.existsSync(path.join(pwd, 'src/server.ts'))) ? path.join(pwd, 'src/server.ts') : path.join(__dirname, 'server.js')

    const env = process.env;
    env.TS_NODE = 'true';
    if (options.inspect) {
      execSync(`yarn nodemon -e ts,json --exec "node --inspect --require ts-node/register ${fileExecPath}"`, { stdio: 'inherit', env });
    } else {
      execSync(`yarn nodemon ` + fileExecPath, { stdio: 'inherit', env });
    }

  });

program
  .command('start')
  .description('launch server')
  .option('--build')
  .action(async (options) => {
    if (options.build || (!fse.existsSync(path.join(pwd, 'dist')) || (fse.existsSync(path.join(pwd, 'dist')) && fse.readdirSync(path.join(pwd, 'dist')).length === 0)))
      await build();

    const fileExecPath = (fse.existsSync(path.join(pwd, 'dist/src/server.js'))) ? path.join(pwd, 'dist/src/server') : path.join(__dirname, 'server')

    await import(fileExecPath)
  });

program.parse(process.argv);
