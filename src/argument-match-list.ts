import { Apiify } from ".";
import { Handler, MatchList, Parameter, RequestMethods } from "./types";
import * as http from "node:http";
import * as _ from "lodash";
import merge from "cat-config/dist/utils/deepmerge"
import { debug, runtime, Runtime } from "./utils";
import { ApiifyServer } from "./apiify-server";
import * as fs from "node:fs";
import * as path from "node:path";


function parseNumberOrThrow(str: string, throwFunc: Function) {
  try {
    str = str.trim();
    if (!/(^[\-0-9][0-9A-Za-z._]+$)|(^[0-9][0-9A-Za-z._]+$|(^[0-9]$))/g.test(str)) throw throwFunc();
    return eval(str);
  } catch (e) {
    throw throwFunc(e);
  }
}

function parseValue(value: any, p: Parameter, app: ApiifyServer) {
  if (value === undefined) return undefined;

  // TODO: deep parseValue
  const dayjs = app.utils.dayjs;

  if (p.type === 'number') {
    return parseNumberOrThrow(value, () => Apiify.ValidationError(`can not parse '${p.name}' to ${p.type}`));
  } else if (p.type === 'boolean') {
    if (['t', 'true', 1].find(v => v === value.toLowerCase())) {
      return true;
    } else if (['f', 'false', 0, -1].find(v => v === value.toLowerCase())) {
      return false;
    } else {
      throw Apiify.ValidationError(`can not parse '${p.name}' to ${p.type}`);
    }
  } else if (p.type === 'Date') {
    const d = new Date(value);
    // @ts-ignore
    if (d == 'Invalid Date') {
      throw Apiify.ValidationError(`can not parse '${p.name}' to ${p.type}`);
    }
    return d;
  } else if (p.type === 'BigInt') {
    return BigInt(value);
  } else if (p.type === 'string') {
    return value;
  } else if (p.type === 'import("open-apiify/src").Apiify.DayJs' || p.type === 'import("dayjs").Dayjs') {
    const d = dayjs(value);
    if (!d.isValid()) throw Apiify.ValidationError(`can't parse '${p.name}' to DayJs`);
    return d;
  } else if (p.type !== 'string' && typeof value !== 'object') {
    try {
      return JSON.parse(value);
    } catch (e) {
      throw Apiify.ValidationError(`can't parse '${p.name}' to json value`);
    }
  } else if (p.type === 'import("open-apiify/src").Apiify.File') {
    const v = value;
    const f = new Apiify.File(v.filepath)
    f.mimetype = v.mimetype
    return f
  } else {
    return value;
  }
}

export default class ArgumentMatchList {
  private list: Array<MatchList> = [];

  constructor() {
    let pkgName = process.cwd().replace(/.*(\/|\\)/i, '');
    if (fs.existsSync(path.resolve(process.cwd(), 'package.json'))) {
      pkgName = require(path.resolve(process.cwd(), 'package.json')).name;
    }

    this.list.unshift({
      matcher: (p) => true,
      async handler(req: http.IncomingMessage, res: http.ServerResponse, p: Parameter, params = {}) {
        // @ts-ignore
        const app = _.get(req, 'app') as ApiifyServer;

        let obj = merge(await Apiify.useRequestJson(req), params);

        let value = _.get(params, p.name, _.get(obj, p.name));

        const contentType = req.headers['content-type'];
        if (contentType && /^multipart\/form\-data;/g.test(contentType)) {
          if (!p.isArray) {
            if (value.length > 1)
              throw Apiify.ValidationError(`expect ${p.type} for '${p.name}'`);
            value = value[0];
          }
        }

        if (!p.isOptional && value === undefined)
          throw Apiify.ValidationError(`require '${p.name}' (${p.type}${p.isArray ? '[]' : ''})`);

        if (!p.isArray && Array.isArray(value))
          throw Apiify.ValidationError(`expect ${p.type} for '${p.name}'`);

        // TODO handle Array
        let v;
        if (p.isArray) {
          if (Array.isArray(value)) {
            v = (value as Array<any>).map(x => parseValue(x, p, app));
          } else if (value !== null && value !== undefined) {
            v = [parseValue(value, p, app)];
          }
        } else {
          v = parseValue(value, p, app);
        }

        return v;
      }
    })

    this.list.unshift({
      matcher: async (p) => {
        if (new RegExp(`import\\(\"${pkgName}\/src\/args\/.*`).test(p.type)) {
          const name = p.type.replace(new RegExp(`import\\(\"${pkgName}\/src\/args\/([a-z]*)\".*`, 'i'), '$1');
          if (fs.existsSync(`${process.cwd()}/src/args/${name}.ts`))
            if (runtime == Runtime.TS_NODE)
              return (await import(`${process.cwd()}/src/args/${name}`)).handler
            else
              return (await import(`${process.cwd()}/dist/src/args/${name}`)).handler
        }
        return false;
      }
    });

    this.list.unshift({
      matcher: p => {
        return (
          p.type == 'ServerResponse' ||
          p.type == 'http.ServerResponse' ||
          p.type == 'import("node:http").ServerResponse' ||
          p.type == 'import("http").ServerResponse'
        );
      },
      handler(req: http.IncomingMessage, res: http.ServerResponse, p: Parameter) {
        return res;
      }
    })

    this.list.unshift({
      matcher: p => {
        return (
          p.type == 'http.IncomingMessage' ||
          p.type == 'IncomingMessage' ||
          p.type == 'import("node:http").IncomingMessage' ||
          p.type == 'import("http").IncomingMessage'
        )
      },
      handler(req: http.IncomingMessage, res: http.ServerResponse, p: Parameter) {
        return req;
      }
    })

    this.list.unshift({
      matcher: p => {
        if (p.type == 'import("open-apiify/src").Apiify.RequestData') {
          return true;
        } else if (p.extends && !!p.extends.find(e => e.text == 'Apiify.RequestData')) {
          return true;
        } else {
          return false;
        }
      },
      async handler(req: http.IncomingMessage, res: http.ServerResponse, p: Parameter) {
        return (req.method == RequestMethods.GET) ?
          Apiify.useQuery(req)
          : await Apiify.useBody(req);
      }
    })

    this.list.unshift({
      matcher: p => {
        return p.type === 'import("open-apiify/src").Apiify.Cookies'
      },
      async handler(req: http.IncomingMessage, res: http.ServerResponse, p: Parameter) {
        return Apiify.useCookies(req);
      }
    })

    this.list.unshift({
      matcher: p => {
        return p.type === 'import("open-apiify/src").Apiify.useSignedCookies'
      },
      async handler(req: http.IncomingMessage, res: http.ServerResponse, p: Parameter) {
        return Apiify.useSignedCookies(req);
      }
    })

    this.list.unshift({
      matcher: p => {
        return p.type === 'import("open-apiify/src").Apiify.HTTPBasicAuth'
      },
      async handler(req: http.IncomingMessage, res: http.ServerResponse, p: Parameter) {
        if (!req.headers['authorization']) {
          if (!p.isOptional) {
            res.setHeader('WWW-Authenticate', 'Basic realm="apiify"');
            throw Apiify.Error(401)
          }
          return undefined;
        } else {
          const arr = req.headers['authorization'].split(' ')
          if (arr[0] !== 'Basic') {
            res.setHeader('WWW-Authenticate', 'Basic realm="apiify"');
            throw Apiify.Error(401)
          }

          let [username, password] = Buffer.from(arr[1], 'base64').toString('utf8').split(':')
          return { username, password }
        }
      }
    })
  }

  insert({
    matcher, handler
  }: MatchList) {
    debug('insert matcher');
    this.list.unshift({ matcher, handler });
  }

  async lookup(p: Parameter): Promise<Handler> {
    for (let l of this.list) {
      let m = await l.matcher(p);
      if (m) {
        return l.handler || m as Handler;
      }
    }

    throw new Error(`\targument type not support:
${_.get(p, '_method._file.path')}\t\t${_.get(p, '_method.name')}\t\t${p.name}: ${p.type}
`);
  }
}
