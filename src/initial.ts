import * as fse from "fs-extra";
import { useConfig } from "./utils/load-config";
import * as _ from "lodash";
import * as path from "node:path";
import { hrtime, debug } from "./utils";

__dirname = __dirname.replace(/\\/, '/');

export default async function initial() {
  hrtime('initial');
  const conf = await useConfig();
  if (!await fse.exists(conf.rootDir + '/node_modules/@types/node'))
    await fse.ensureSymlink(__dirname + '/../node_modules/@types/node'.replace(/\\/, '/'), conf.rootDir + '/node_modules/@types/node');

  if (!await fse.exists(conf.rootDir + '/node_modules/open-apiify'))
    await fse.ensureSymlink(__dirname + '/..'.replace(/\\/, '/'), conf.rootDir + '/node_modules/open-apiify');

  let p = await require(__dirname + '/../package.json')


  const npmPackageFilePath = conf.rootDir + '/package.json';
  let x = await fse.openSync(npmPackageFilePath, 'w+');
  let y = (await fse.readJson(npmPackageFilePath, { throws: false }) || {});
  _.set(y, 'scripts.dev', 'apiify dev');
  if (conf.rootDir)
    _.set(y, 'name', _.get(y, 'name', path.basename(conf.rootDir)));
  _.set(y, 'dependencies.open-apiify', _.get(p, 'version'))
  _.set(y, 'devDependencies.@types/node', _.get(p, 'devDependencies.@types/node'))
  await fse.writeJson(npmPackageFilePath, y, { spaces: 2 });
  await fse.close(x);

  debug(`initial: ${hrtime('initial')}`);
}
