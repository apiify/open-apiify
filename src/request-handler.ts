import * as http from "http";
import { ApiifyServer } from "./apiify-server";
import Apiify from ".";
import * as url from "node:url";
import { hrtime, debug } from "./utils";
import * as Cookie from "cookie";
import * as Stream from "node:stream";
import HttpStatusCode from "./utils/http-status";
import _ = require("lodash");

function setCookie(req: http.IncomingMessage, res: http.ServerResponse) {
  // @ts-ignore
  if (req.__cookies) {
    // @ts-ignore
    const cookies = req.__cookies as Apiify.Cookies;
    let str = '';
    for (const key of Object.keys(cookies).filter(v => v !== '__options__')) {
      str += Cookie.serialize(key, cookies[key], cookies[key].__options__?.[key]) + ';';
    }
    res.setHeader('Set-Cookie', str);
  }
}

export default function (app: ApiifyServer) {
  return async function (
    req: http.IncomingMessage, res: http.ServerResponse<http.IncomingMessage> & {
      req: http.IncomingMessage
    }) {
    if (process.env.NODE_ENV !== 'test')
      await app.waitForReady();

    let result: any
    try {
      // check request length limit
      if (req.headers['content-length'] && (app.options.request?.limitSize)) {
        const length = parseInt(req.headers['content-length']);
        if ((
          req.headers['content-type']
          && /^multipart\/form\-data;/g.test(req.headers['content-type'])
          && app.options.request.uploadLimitSize
          && length > app.options.request.uploadLimitSize
        )
          || (
            req.headers['content-type']
            && app.options.request.limitSize
            && length > app.options.request.limitSize
          )) {
          throw Apiify.Error(413)
        }
      }

      // /healthcheck
      if (req.url === '/healthcheck') {
        res.end('OK');
        return
      }

      _.set(req, 'res', res);
      _.set(req, 'app', app);
      const dummy = Math.random() + '';
      hrtime(dummy);
      const u = url.parse(req.url || '/', true);

      let pathname = u.pathname || '/';
      let r = new RegExp('^' + app.options.basePath, 'gm')
      if (!r.test(pathname)) {
        res.end();
        return;
      }

      debug(`-------- ${req.method}: ${u.pathname} --------`);

      pathname = pathname.replace(r, '/');
      hrtime('route match')
      const routes = app.router.match(req.method || 'GET', pathname);
      debug(`route match: ` + hrtime('route match'));
      if (routes.length == 0) {
        debug(`No route found for ${req.method} ${pathname}`);
        res.statusCode = 404;
        return;
      }

      res.statusCode = 200;
      for (const i in routes) {
        const route = routes[i];
        if (!res.writableObjectMode && !res.writableEnded) {
          let rxs: any = [];
          hrtime('http_req_' + i);
          if (route.args.length > 0) {
            rxs = await Promise.all(
              route.args.map((fn: Function, i: number) => {
                return fn(req, res, route.parameters[i], route.params)
              })
            );
          } else {
            rxs = [req, res];
          }
          debug(`map args: ${hrtime('http_req_' + i)}`);

          debug(`call ${route.methods[0]}: ${route.file}`);

          result = await route.func.apply(res, rxs);

          debug(`call ${route.methods[0]}: ${route.file} in ${hrtime('http_req_' + i)}`);

          if (parseInt(i) >= routes.length - 1 && result === undefined) {
            setCookie(req, res);
            res.setHeader("Content-Type", "application/json");
            res.write(JSON.stringify({ success: true }));
            res.end();
            return;
          } else if (result !== null && result !== undefined) {
            setCookie(req, res);

            if (result instanceof Promise)
              result = await result

            if (result instanceof Apiify.File) {
              await Apiify.sendFile(req, result.absolutePath, { compress: result.options?.compress || false });
            } else if (result instanceof Stream.Readable) {
              result.pipe(res);
            } else {
              await app.responseEncoding(req, res, result);
            }

            res.end();
            break;
          } else {
            res.statusCode = 404;
          }
        }
      }
    } catch (e) {
      app.errorHandler(req, res, e as Error)
    } finally {
      if (!res.headersSent)
        res.write(HttpStatusCode[res.statusCode] || 'Unassigned');

      // @ts-ignore
      res.status = res.statusCode;

      res.end();
    }
  }
}