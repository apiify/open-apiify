import { Router } from ".";

describe('router', () => {
  test('01', () => {
    const route = new Router();
    route.add('GET', '/', { p: '/' });

    expect(route.treeRoutes).toMatchInlineSnapshot(`
{
  "": {
    "GET": [
      {
        "file": undefined,
        "params": {},
        "payload": {
          "p": "/",
        },
      },
    ],
  },
}
`);

    const r = route.match('GET', '/');
    expect(r).toMatchInlineSnapshot(`
[
  {
    "file": undefined,
    "p": "/",
    "params": {},
  },
]
`);
  });

  test('02', () => {
    const route = new Router();
    route.add('GET', '/foo/bar', { p: '/foo/bar' });
    route.add('GET', '/foo', { p: '/foo' });
    route.add('GET', '/foo/:id', { p: '/foo/:id' });
    route.add('GET', '/foo/**', { p: '/foo/**' });

    expect(route.treeRoutes).toMatchInlineSnapshot(`
{
  "foo": {
    "**": {
      "GET": [
        {
          "file": undefined,
          "params": {
            "1": "id",
          },
          "payload": {
            "p": "/foo/:id",
          },
        },
        {
          "file": undefined,
          "params": {},
          "payload": {
            "p": "/foo/**",
          },
        },
      ],
    },
    "GET": [
      {
        "file": undefined,
        "params": {},
        "payload": {
          "p": "/foo",
        },
      },
    ],
    "bar": {
      "GET": [
        {
          "file": undefined,
          "params": {},
          "payload": {
            "p": "/foo/bar",
          },
        },
      ],
    },
  },
}
`);

    const r = route.match('GET', '/foo/bar');
    expect(r).toMatchInlineSnapshot(`
[
  {
    "file": undefined,
    "p": "/foo/:id",
    "params": {
      "id": "bar",
    },
  },
  {
    "file": undefined,
    "p": "/foo/**",
    "params": {},
  },
  {
    "file": undefined,
    "p": "/foo/:id",
    "params": {
      "id": "bar",
    },
  },
  {
    "file": undefined,
    "p": "/foo/**",
    "params": {},
  },
  {
    "file": undefined,
    "p": "/foo/bar",
    "params": {},
  },
]
`);
  });

  test('03', () => {
    const route = new Router();
    route.add('GET', '/foo/bar', { p: '/foo/bar' });
    route.add('GET', '/foo', { p: '/foo' });
    route.add('GET', '/foo/:id', { p: '/foo' });
    route.add('GET', '/foo/**', { p: '/foo/**' });

    const r = route.match('GET', '/foo');
    expect(r).toMatchInlineSnapshot(`
[
  {
    "file": undefined,
    "p": "/foo",
    "params": {},
  },
]
`);
  });

  test('04', () => {
    const route = new Router();
    route.add('GET', '/foo/bar', { p: '/foo/bar' });
    route.add('GET', '/foo', { p: '/foo' });
    route.add('GET', '/foo/:id/bar/:id2', { p: '/foo/:id/bar/:id2' });
    route.add('GET', '/foo/**', { p: '/foo/**' });
    expect(route.regexRoutes).toMatchInlineSnapshot(`
[
  [
    /\\^\\\\/foo\\$/,
    {
      "GET": [
        {
          "file": undefined,
          "params": {},
          "payload": {
            "p": "/foo",
          },
        },
      ],
    },
    "/foo",
  ],
  [
    /\\^\\\\/foo\\\\/\\.\\*\\$/,
    {
      "GET": [
        {
          "file": undefined,
          "params": {},
          "payload": {
            "p": "/foo/**",
          },
        },
      ],
    },
    "/foo/**",
  ],
  [
    /\\^\\\\/foo\\\\/\\[\\^/\\]\\*\\\\/bar\\\\/\\[\\^/\\]\\*\\$/,
    {
      "GET": [
        {
          "file": undefined,
          "params": {
            "1": "id",
            "3": "id2",
          },
          "payload": {
            "p": "/foo/:id/bar/:id2",
          },
        },
      ],
    },
    "/foo/:id/bar/:id2",
  ],
  [
    /\\^\\\\/foo\\\\/bar\\$/,
    {
      "GET": [
        {
          "file": undefined,
          "params": {},
          "payload": {
            "p": "/foo/bar",
          },
        },
      ],
    },
    "/foo/bar",
  ],
]
`);

    const r = route.match('GET', '/foo/x1/bar/x2');
    expect(r).toMatchInlineSnapshot(`
[
  {
    "file": undefined,
    "p": "/foo/**",
    "params": {},
  },
  {
    "file": undefined,
    "p": "/foo/:id/bar/:id2",
    "params": {
      "id": "x1",
      "id2": "x2",
    },
  },
]
`);
  });

  test('05', () => {
    const route = new Router();
    route.add('GET', '/v1/users/:id', { p: '/v1/users/**' });
    expect(route.treeRoutes).toMatchInlineSnapshot(`
{
  "v1": {
    "users": {
      "**": {
        "GET": [
          {
            "file": undefined,
            "params": {
              "2": "id",
            },
            "payload": {
              "p": "/v1/users/**",
            },
          },
        ],
      },
    },
  },
}
`);
    const r = route.match('GET', '/v1/users/me');
    expect(r).toMatchInlineSnapshot(`
[
  {
    "file": undefined,
    "p": "/v1/users/**",
    "params": {
      "id": "me",
    },
  },
]
`);
  })
});