import * as _ from "lodash";
import Debug from "debug";

const debug = Debug('apiify:router');

function x(routes: any, arr: string[], method: string, container: any, payload: any) {
  let c = _.get(container, `${arr[0]}.${method}`, {})

  if (arr[1]) {
    arr.shift();
    let y2 = x(routes, arr, method, c, payload)

    let y3 = {};
    // @ts-ignore
    y3[arr[0]] = y2;


  } else {
    let y1 = container[arr[0]] || [];
    y1[method] = y1[method] || [];
    y1[method].push(payload);
    container[arr[0]] = y1;
  }

  return container;
}

export class Router<T> {
  private isParamsMode = false;
  linearRoutes: {
    [field: string]: T[];
  } = {};

  treeRoutes: {
    [field: string]: any;
  } = {}

  regexRoutes: Array<[RegExp, any, string]> = [];

  temp: any = {};

  add(method: string, path: string, payload: T = {} as T, file?: string) {
    this.isParamsMode = true;
    let p = path.substring(1, path.length).replace(/\:[^\/]+/gm, '**').replace(/\/$/gm, '').replace(/\//gm, '.');
    let container = _.get(this.treeRoutes, p + '.' + method) || [];
    const params: any = {}

    const arr = _.compact(path.split('/'));
    for (const i in arr) {
      if (/^\:/gm.test(arr[i]))
        params[i] = arr[i].substring(1, arr[i].length);
    }


    container.push({ payload, params, file });
    _.set(this.treeRoutes, p + '.' + method, container);

    //--------------------------------------------------
    {
      let p = path
        .replace(/\:[^\/]+/gm, '[^\/]*')
        .replace(/\/$/gm, '')
        .replace(/\*\*/gm, '.*');

      this.regexRoutes.push([new RegExp(`^${p}$`), {
        [method]: container
      }, path]);

      this.regexRoutes = this.regexRoutes.sort((a, b) => {
        if (a[2] > b[2]) {
          return 1
        } else if (a[2] == b[2]) {
          return a[1]['ALL'] ? -1 : 1;
        } else {
          return -1
        }
      })
    }
  }

  match(method: string, path: string) {
    path = path.replace(/\/$/, '');
    let routes: any[] = [];
    let regexRoutes = this.regexRoutes;
    let pa = path.substring(1, path.length).split('/');
    for (const regexRoute of regexRoutes) {

      if (regexRoute[0].test(path)) {
        let arr = []
        regexRoute[1]['ALL'] && arr.push(...regexRoute[1]['ALL']);
        regexRoute[1][method] && arr.push(...regexRoute[1][method]);

        arr.map(c => {
          let params: any = {}
          if (c.params) {
            for (const i in pa) {
              if (c.params[i])
                params[c.params[i]] = pa[i];
            }
          }
          routes.push({ ...c.payload, params: params, file: c.file })
        })
      }
    }

    return routes;
  }
}
