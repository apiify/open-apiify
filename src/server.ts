// @ts-nocheck
import { Apiify } from ".";


(async () => {
  const pwd = process.cwd().replace(/\\/gm, '/');
  const app = await Apiify.create(pwd);

  app.listen();
})().catch(e => { throw e });
