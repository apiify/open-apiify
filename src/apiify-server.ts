import * as http from "http";
import * as https from "https";
import { ListenOptions } from "net";
import { ApiifyRouteFile, InnerOptions, MatchList, Options, Parameter, RequestMethods, RouteContent } from "./types";
import { hrtime, debug, stackTrace, listFiles, getAcceptEncodingHeader, Runtime, runtime } from "./utils";
import * as _ from "lodash";
import ArgumentMatchList from "./argument-match-list";
import { ApiifyError, ValidationErrorClass } from ".";
import * as Stream from "node:stream";
import * as path from "node:path";
import RouteFile from "./utils/apiify-route-file";
import * as fse from "fs-extra";
import HttpStatusCode from "./utils/http-status";
import * as cluster from "node:cluster";
import bytes from "bytes";
import * as os from "node:os";
import dayjs from "dayjs";
import { Router } from "./router";
import RequestHandler from "./request-handler";
import httpsCert from "./utils/https-cert";
import Apiify from ".";
import staticFile, { StaticOptions } from "./static-file";

function join(...args: string[]) {
  return path.join(...args).replace(/\\/g, "/");
}

export function optionsTransfer(options: Options): InnerOptions {

  const pwd = process.cwd().replace(/\\/gm, '/');
  const op: InnerOptions = {
    basePath: '/',
    ...options,
    rootDir: options.rootDir || pwd,
    main: '',
    listen: {
      port: 80,
      host: '127.0.0.1',
      ...options.listen
    },
    request: {
      limitSize: bytes(options.request?.limitSize || '100KB'),
      uploadLimitSize: bytes(options.request?.uploadLimitSize || '10MB')
    },
    server: {
      worker: process.env.NODE_ENV === 'production' ? options.server?.productionWorker : options.server?.worker
    },
  };

  if (runtime === Runtime.NODEJS) {
    op.routeDir = join(op.rootDir || pwd, op.routeDir || 'dist/routes');
    op.argsFilePath = join(op.rootDir || pwd, op.argsFilePath || 'dist/args/index');
  } else {
    op.routeDir = join(op.rootDir || pwd, 'src/routes');
    op.argsFilePath = join(op.rootDir || pwd, op.argsFilePath || 'src/args/index');
  }

  if (options.https?.enable) {
    op.https = {
      enable: true,
      keyFile: join(op.rootDir || pwd, options.https?.keyFile || 'cert/key.pem'),
      certFile: join(op.rootDir || pwd, options.https?.certFile || 'cert/cert.pem'),
      redirectPort: options.https?.redirectPort
    }

    const caFile = join(op.rootDir || pwd, options.https?.caFile || 'cert/ca.crt')
    if (fse.existsSync(caFile))
      op.https.caFile = caFile;

  }

  return op
}

export class ApiifyServer {

  options: InnerOptions;
  server: http.Server | https.Server;
  matcher = new ArgumentMatchList();
  router = new Router();

  utils = {
    dayjs
  }

  public _serverListened?: Promise<void>;
  private _haveProcessWorker = false;
  private _readyPromise: Promise<void>;
  private ready = () => { };

  constructor(options: InnerOptions = {} as InnerOptions) {

    this.options = options;

    if (this.options.server?.worker)
      this.createWorkerProcess(this.options.server?.worker);

    hrtime('server create');

    if (this.options.https?.enable) {
      let ops = httpsCert(join(this.options.rootDir, 'cert'), { installCa: true, domains: ['127.0.0.1', 'localhost'] });

      this.server = https.createServer(ops, RequestHandler(this));
    } else {
      this.server = http.createServer(RequestHandler(this));
    }

    this._readyPromise = new Promise((resolve, reject) => {
      this.ready = resolve;
    });

    debug('server create: %s', hrtime('start server'));

    if (process.env.NODE_ENV !== 'test')
      this.startServer();
  }

  waitForReady() {
    return this._readyPromise;
  }

  errorHandler(req: http.IncomingMessage, res: http.ServerResponse, e: Error) {
    if (e instanceof ValidationErrorClass) {
      res.statusCode = 400;
      res.write((e as ValidationErrorClass).message);
    } else if (e instanceof ApiifyError) {
      res.statusCode = (e as ApiifyError).code;
      res.write((e as ApiifyError).message || HttpStatusCode[res.statusCode] || 'Unassigned');
    } else {
      res.statusCode = 500;
      const error = e as Error;
      if (error.stack)
        res.write(error.stack?.toString());
      else
        res.write('');
    }

    if (res.statusCode == 500 || process.env.NODE_ENV !== 'test')
      console.error(e);

    res.end();
    req.destroy();
  }

  responseEncoding(req: http.IncomingMessage, res: http.ServerResponse, data: any | string | Stream.Readable) {

    if (typeof data === 'object') {
      res.setHeader("Content-Type", "application/json");
      if (data.toJSON)
        data = JSON.stringify(data.toJSON());
      else if (data.toJson)
        data = JSON.stringify(data.toJson());
      else
        data = JSON.stringify(data);
    }
    debug(`format res: ${hrtime('http_req')}`);

    // TODO: add to config
    if (data.length < 1024) {
      res.write(data);
      res.end();
      return;
    }

    return new Promise(async (resolve) => {
      const zlib = await import('zlib');
      let acceptEncods = getAcceptEncodingHeader(req);

      for (const acceptEncod of acceptEncods) {
        if (acceptEncod == 'br') {
          res.setHeader('Content-Encoding', 'br');
          let inputBuffer;
          if (Buffer.isBuffer(data)) {
            inputBuffer = data;
          } else {
            inputBuffer = Buffer.from(data.toString(), 'utf8');
          }

          zlib.brotliCompress(inputBuffer, (err, compressedBuffer) => {
            if (err)
              throw Apiify.Error(500, err.message);

            res.write(compressedBuffer);
            res.end();
            resolve(null);
          });
          break;
        }
      }
    });
  }

  startServer(listenOptions?: ListenOptions) {
    if (this.options.https?.enable && this.options.https?.redirectPort) {
      http.createServer((req, res) => {
        res.writeHead(302, {
          'Location': `https://${(req.headers.host || 'localhost').replace(/:.*/, '')}:${this.options.listen.port || 443}${req.url}`
        });
        res.end();
      })
        .listen(this.options.https.redirectPort);
    }

    // @ts-ignore    
    if ((cluster.isPrimary && !this._haveProcessWorker) || (cluster.isWorker))
      this._serverListened = new Promise<void>((resolve, reject) => {
        this.server.listen({
          ...this.options.listen,
          ...listenOptions
        }, resolve);
      });


    return this._serverListened;
  }

  async listen() {
    if (process.env.NODE_ENV === 'test')
      await this.startServer();
    this.ready();

    // @ts-ignore    
    if (cluster.isPrimary)
      console.info(`server start on port ` + (this.options.listen.port || 80));
  }

  async route(
    method: RequestMethods = RequestMethods.ALL,
    path: string,
    func: Function,
    parameters: Parameter[] = [],
    file?: string
  ): Promise<ApiifyServer> {
    path = path.replace(/(\[)(\w+)(\])/gmi, ':$2')
      .replace(/(\[)(\.{3}\w+)(\])/gmi, '**:$2');
    debug(`route add: ${method} ${path}`);

    const args = parameters.length > 0 ? await Promise.all(parameters.map(async p => this.matcher.lookup(p)))
      : [(req: http.IncomingMessage) => req, (_req: http.IncomingMessage, res: http.ServerResponse) => res]

    this.router.add(
      method,
      path,
      {
        path: path as string,
        methods: [method],
        func,
        parameters,
        args
      },
      file
    );

    return this;
  }

  all(
    path: string,
    func: (req: http.IncomingMessage, res: http.ServerResponse) => any,
    file?: string
  ) {
    this.route(RequestMethods.ALL, path, func, [], file);
  }

  get(
    path: string,
    func: (req: http.IncomingMessage, res: http.ServerResponse) => any,
    file?: string
  ) {
    this.route(RequestMethods.GET, path, func, [], file);
  }

  post(
    path: string,
    func: (req: http.IncomingMessage, res: http.ServerResponse) => any,
    file?: string
  ) {
    this.route(RequestMethods.POST, path, func, [], file);
  }

  /**
   * 
   * @param amount default is os.cpus.length
   */
  private async createWorkerProcess(amount?: number, env?: any) {
    this._haveProcessWorker = true;
    // @ts-ignore
    if (cluster.isPrimary) {
      debug(`create worker precess ${amount}`);
      if (this._serverListened) {
        await this._serverListened;
        await new Promise(r => this.server.close(r));
      }

      if (amount === undefined || amount === -1) amount = os.cpus().length
      for (let i = 0; i < amount; i++) {
        // @ts-ignore
        const worker = cluster.fork(env);
        debug(`worker ${worker.process.pid} created`);
      }
    }
  }

  async loadRouteDir(routeDir: string, options: InnerOptions) {
    let apiifyRouteFiles: ApiifyRouteFile[];
    debug('runtime: %s', runtime);
    if (runtime !== Runtime.NODEJS) {
      if (!path.isAbsolute(routeDir))
        routeDir = path.resolve(path.dirname(stackTrace()[6]), routeDir);

      debug(`routes file from: ${routeDir}`);
      if (!(await fse.exists(routeDir))) {
        throw `routes dirtory not found: ${routeDir}`;
      }

      let files = await listFiles(routeDir);

      apiifyRouteFiles =
        (await Promise.all(
          files.map(async (file) => {
            return RouteFile.create(routeDir, file);
          })
        ))
          .sort((a, b) => {
            // ex. ped.ts and ped.post.ts
            if (a.path == b.path)
              return a.file.length - b.file.length;

            const x = a.path.replace(/[^\/]/gmi, '').length - b.path.replace(/[^\/]/gmi, '').length;
            const y = a.path.length - b.path.length;
            return x !== 0 ? x : y;
          });
    } else {
      const p = path.resolve(options.rootDir, 'dist/routes.json');

      debug(`load file: ` + p);
      apiifyRouteFiles = await fse.readJSON(p);

      apiifyRouteFiles = await Promise.all(
        apiifyRouteFiles
          .map(async f => {
            const p = path.join(this.options.rootDir || '', 'dist/src/routes', f.file || '').replace(/\.[^\.]*$/, '');

            const funcs = await import(p);
            f.methods = f.methods.map(m => {
              let name = m.name.toLowerCase();
              if (name == 'delete') name = 'del';

              // @ts-ignore
              m.func = funcs[name];
              if (!m.func)
                m.func = funcs['default'];

              return m;
            });
            return f
          })
      );

      // const funcs = (await import(path.join(routeDir, file.path).replace(/\.[^\.]+$/g, '')));
    }

    for (const arf of apiifyRouteFiles) {
      for (const m of arf.methods) {
        if (/\.[^\.\[\]]+\.ts$/i.test(arf.file)) {
          await this.route(
            arf.file.replace(/.*\.([^\.]+)\.ts$/i, '$1').toUpperCase() as RequestMethods,
            arf.path,
            m.func,
            m.parameters,
            arf.file
          );
        } else {
          const name = m.name == 'default' ? 'ALL' : m.name.toUpperCase();
          await this.route(
            name as RequestMethods,
            arf.path,
            m.func,
            m.parameters,
            arf.file
          );
        }
      }
    }
  }

  async loadArgumentMatchList(argsFilePath: string) {
    if (!path.isAbsolute(argsFilePath))
      argsFilePath = path.resolve(path.dirname(stackTrace()[7]), argsFilePath);

    if (await fse.exists(argsFilePath + '.js') || await fse.exists(argsFilePath + '.ts')) {
      debug(`load argument match list: ${argsFilePath}`);

      let func = runtime == Runtime.TS_NODE ?
        (await import(argsFilePath.replace(/\\/gm, '/'))).default :
        (await import(path.resolve(argsFilePath.replace(/\\/gm, '/'), '../../../../dist/src/args/index'))).default;

      (await func(this) as MatchList[])
        .reverse()
        .map(v => this.matcher.insert(v))
    }
    // throw `routes dirtory not found: ${argsFilePath}`;
  }

  static(dirPath: string, options: StaticOptions = {} as StaticOptions) {
    if (!path.isAbsolute(dirPath)) {
      dirPath = path.resolve(path.dirname(stackTrace()[1]), dirPath);
    }
    return staticFile(this, dirPath, options)
  }
}
