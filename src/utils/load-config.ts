import { ListenOptions } from "node:net";
import merge from "cat-config/dist/utils/deepmerge";
import * as fse from "fs-extra";
import * as path from "path";
import { Options } from "../types/index";
import { debug, runtime, Runtime } from ".";
import Config from "cat-config";
import * as _ from "lodash";

let conf: Options

export default function loadConfig(rootDir?: string): Options {
  if (!conf) {
    rootDir = path.resolve(rootDir || process.cwd()).replace(/\\/g, '/');
    debug(`rootDir: ${rootDir}`);

    debug('load config')
    let c: Options = {
      listen: {
      },
      rootDir,
      routeDir: 'src/routes',
      argsFilePath: 'src/args/index',
      apiifyWorkPath: '.apiify'
    }

    const mainPath = runtime == Runtime.NODEJS ?
      path.resolve(rootDir, '../config') :
      rootDir + '/config';

    debug('configDir: ' + mainPath);

    c = Config<Options>(c, { mainPath })
      .js()
      .loadEnv({ envName: 'APIIFY_CONFIG' })
      .env()
      .argv()
      .execSync();

    if (!c.listen?.port) {
      if (c.https?.enable) {
        _.set(c, 'listen.port', 443);
        if (!c.https?.redirectPort)
          _.set(c, 'https.redirectPort', 80);
      } else {
        _.set(c, 'listen.port', 80);
      }
    }

    conf = c;
  }
  debug('load config done');
  return conf;
}

export const useConfig = loadConfig
