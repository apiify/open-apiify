import * as fse from "fs-extra";
import * as path from "node:path";
import Debug from "debug";
import { Project, SourceFile, FunctionDeclaration, FunctionExpression, Node, Type, ts, ParameterDeclaration } from "ts-morph";
import * as process from 'node:process';
import { mime } from "send";
import * as contentType from "content-type";
import { IncomingMessage } from "node:http";

const project = new Project();

export async function getSourceFile(absoluteFilePath: string) {
  return project.addSourceFileAtPath(absoluteFilePath);
}

export function findExportFunctions(sourceFile: SourceFile): FunctionDeclaration[] {
  const exportFunctions: FunctionDeclaration[] = [];
  sourceFile.forEachChild((node: any) => {
    if (
      Node.isFunctionDeclaration(node)
      && (node as FunctionDeclaration).isExported()
    ) {
      exportFunctions.push(node);
    }
  });

  return exportFunctions;
}

function getSourceFileFromType(type: Type) {
  // Get the symbol associated with the type
  const symbol = type.getSymbol() || type.getAliasSymbol();
  // @ts-ignore
  let s = symbol.compilerSymbol.parent




  if (!symbol) return undefined;

  // Get the declaration associated with the symbol
  const declaration = symbol.getDeclarations()?.[0];
  if (!declaration) return undefined;

  // Return the source file of the declaration
  return declaration.getSourceFile();
}

/**
 * @deprecated
 */
export async function getParametersType(sourceFile: SourceFile) {
  const defaultExport = sourceFile.getDefaultExportSymbol();

  // Get the default export's value declaration and extract its text
  const defaultExportValue = defaultExport?.getValueDeclaration();
  if (!defaultExportValue) {
    throw new Error("Default export value not found.");
  }

  // Check if it's a function declaration or a function expression
  if (Node.isFunctionDeclaration(defaultExportValue) || Node.isFunctionExpression(defaultExportValue)) {
    const functionNode = defaultExportValue as FunctionDeclaration | FunctionExpression;

    // Get function parameters
    const parameters = functionNode.getParameters();

    let x = parameters.map((p: any) => getSourceFileFromType(p.getType())?.getFilePath());//.map(t => t.getText())

    let params = await Promise.all(
      parameters.map(async (p: any) => ({
        parameter: p,
        typeText: await parseImportString(p.getType().getText()),
      }))
    )

    return params;

  } else {
    throw new Error("Default export is not a function.");
  }
}


// Function to list files in a deep folder
export async function listFiles(dir: string, routeDir?: string): Promise<Array<{ path: string; stat: fse.Stats }>> {
  routeDir = routeDir || dir;
  dir = dir.replace(/\\/g, '/');

  const list = [];
  const files = await fse.readdir(dir);
  for (const file of files.map(f => path.join(dir, f).replace(/\\/gmi, '/'))) {
    const stat = await fse.stat(file);

    // ignore file or directory name start with '_'
    if (!/_[^\/]+$/.test(file) && !/\.(spec|test)\.ts$/.test(file))
      if (stat.isDirectory()) {
        list.push(...(await listFiles(file, routeDir)));
      } else {
        list.push({ path: path.relative(routeDir, file).replace(/\\/gmi, '/'), stat });
      }
  }

  return list
}

export async function parseImportString(pt: string) {
  if (/ \| /.test(pt)) {
    throw new Error(`Parameter type "${pt}" is not supported.`);
  }

  pt = pt
    .replace(/<.*>/gmi, '')
    .replace(/(import\(")(.*\/node_modules\/)(.*)/, "$1$3")


  if (/^import\("[a-zA-Z]:/.test(pt)) {
    let p = pt.replace(/(import\(")(.*)(".*)/, "$2");
    while (!(await fse.exists(path.join(p, 'package.json')))) {
      p = path.dirname(p);
    }

    let name = require(path.join(p, 'package.json')).name

    pt = pt.replace(p, name);
  }

  pt = pt
    .replace('"@types/', '"')
    .replace(/\.default$/, '')
    .replace(/\/index\"/, '"')

  return pt
}

export const debug = Debug('apiify')

let hrtimeBefore = {};
export function hrtime(name = '') {
  let h = Math.floor(process.hrtime()[1] / 1e3);
  // @ts-ignore  
  return (hrtimeBefore[name] = h - (hrtimeBefore[name] || 0))
    .toString()
    .replace(/(\d{1,3})(\d{3})$/g, '$1_$2') + 'µs';
}

export function logMemmoryUse() {
  const used = process.memoryUsage();
  return `memory usage: ${(Math.round(used.heapUsed / 1024 * 100) / 100).toString().replace(/(\d{3})\./, ',$1.')} KB`
}

export function getExtendsText(parameter: ParameterDeclaration): Array<{ text: string }> {
  try {
    // @ts-ignore
    return parameter.getTypeNode().getType().getSymbol()?.getDeclarations()?.[0]?.getExtends()?.map(e => e.getText()).map(t => ({ text: t }));
  } catch (e) {
    // console.warn(e);

    return [] as Array<{ text: string }>
  }
}

export function stackTrace() {
  let stacks = [];
  try {
    throw new Error('');
  } catch (e) {
    stacks = ((e as Error).stack || '').split('\n')
      // .filter(s => !/\(/.test(s));
      .filter(s => !/\(\<anonymous\>\)/gmi.test(s));

    if (stacks.length > 0) {
      stacks.shift();
      stacks.shift();
    }

    return stacks
      .map(
        s => s.trim().replace(/^at /g, '')
          .replace(/\:\d+\:\d+/gm, '')
          .replace(/.*\((.*)\).*$/gi, '$1')
      );
  }
}

export function getAcceptEncodingHeader(req: IncomingMessage) {
  const encoding = req.headers['accept-encoding'] as string;
  if (encoding) {
    return encoding.split(',').map(type => type.trim()).sort((a, b) => {
      const qualityA = parseFloat(a.split(';q=')[1] || '1.0');
      const qualityB = parseFloat(b.split(';q=')[1] || '1.0');
      return qualityB - qualityA;
    })
  } else {
    return [] as string[];
  }
}

export enum Runtime {
  DENO = 'DENO',
  TS_NODE = 'TS_NODE',
  NODEJS = 'NODEJS',
  UNKNOWN = 'UNKNOWN'
}

export const runtime: Runtime = (() => {
  // @ts-ignore
  if (typeof Deno !== 'undefined') {
    return Runtime.DENO;
  } else if (typeof process !== 'undefined' && process.versions && process.versions.node) {
    if (process.env.TS_NODE == 'true' || /node_modules\\ts-node\\dist\\bin\.js$/i.test(process.argv[0]) || process.env.NODE_ENV === 'test')
      return Runtime.TS_NODE
    else
      return Runtime.NODEJS;
  } else {
    return Runtime.UNKNOWN;
  }
})();

export default function () { }
