import * as fs from "node:fs";
import * as path from "node:path";
import * as shell from "shelljs";
import { consola } from "consola";

interface Options { installCa: boolean, domains: string[] }

function initCredentials(certDir: string, obs: Options = { installCa: false, domains: [] }) {
  shell.mkdir('-p', certDir)

  obs = Object.assign({ domains: ['127.0.0.1', 'localhost'], installCa: true }, obs)

  let ca
  const keyFilePath = path.resolve(certDir, 'key.pem')
  const certFilePath = path.resolve(certDir, 'cert.pem')

  if (!fs.existsSync(keyFilePath) || !fs.existsSync(certFilePath)) {
    if (!shell.which('mkcert')) {
      consola.box(`press install mkcert from https://github.com/FiloSottile/mkcert`)
      process.exit(1);
    }
    shell.exec(['mkcert', '-key-file', keyFilePath, '-cert-file', certFilePath, ...obs.domains].join(' '), { silent: true })
    obs.installCa && shell.exec('mkcert -install', { silent: false })
  }
  ca = {
    key: fs.readFileSync(keyFilePath, 'utf8'),
    cert: fs.readFileSync(certFilePath, 'utf8')
  }
}

function getCredentials(certDir: string) {
  let obj: {
    key: Buffer,
    cert: Buffer,
    ca?: Buffer
  }

  if (fs.existsSync(path.resolve(certDir, 'key.csr')) && fs.existsSync(path.resolve(certDir, 'cert.crt'))) {
    obj = {
      key: fs.readFileSync(path.resolve(certDir, 'key.csr')),
      cert: fs.readFileSync(path.resolve(certDir, 'cert.crt')),
    }
    if (fs.existsSync(path.resolve(certDir, 'ca.crt')))
      obj.ca = fs.readFileSync(path.resolve(certDir, 'ca.crt'))
    return obj;
  } else if (fs.existsSync(path.resolve(certDir, 'key.pem')) && fs.existsSync(path.resolve(certDir, 'cert.pem'))) {
    obj = {
      key: fs.readFileSync(path.resolve(certDir, 'key.pem')),
      cert: fs.readFileSync(path.resolve(certDir, 'cert.pem'))
    }
    if (fs.existsSync(path.resolve(certDir, 'ca.crt')))
      obj.ca = fs.readFileSync(path.resolve(certDir, 'ca.crt'))

    return obj;

  } else {
    console.warn(`ssl file not found`)
    return {
      key: fs.readFileSync(path.resolve(__dirname, '../ssl', 'key.pem')),
      cert: fs.readFileSync(path.resolve(__dirname, '../ssl', 'cert.pem'))
    };
  }
}

export default function (certDir: string, options: Options = { installCa: false, domains: [] }) {
  options = {
    ...{
      installCa: true
    },
    ...options
  }

  initCredentials(certDir, options)
  return getCredentials(certDir)
}
