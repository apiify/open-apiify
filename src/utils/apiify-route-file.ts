import * as fse from "fs-extra";
import * as path from "node:path";
import { getSourceFile, findExportFunctions, parseImportString, getExtendsText } from ".";
import { ApiifyRouteFile, Options, Parameter } from "../types";

export default {
  // TODO: why this function is call twice?
  async create(routeDir: string, file: { path: string, stat: fse.Stats }, isBuild = false): Promise<ApiifyRouteFile> {
    const sourceFile = await getSourceFile(path.join(routeDir, file.path));
    const funcs = findExportFunctions(sourceFile)
      .filter(f => !f.getName() || ['all', 'connect', 'del', 'get', 'head', 'options', 'post', 'put', 'trace'].find(m => m == f.getName()));
    const methods: Array<{
      name: string,
      func: Function,
      parameters: Parameter[]
    }> = [];
    for (const func of funcs) {
      const name = func.getName() || 'all';

      const parameters: Parameter[] =
        await Promise.all(
          func.getParameters()
            .map(async (p: any) => {
              let type = await parseImportString(p.getType().getText());
              const isArray = /\[\]$/g.test(type);
              type = type.replace(/\[\]$/g, '');
              return new Parameter({
                name: p.getName(),
                rawType: p.getType().getText(),
                type,
                isOptional: p.isOptional(),
                extendsList: getExtendsText(p),
                isArray
              })
            })
        )

      let f = () => { };
      if (!isBuild) {
        const funcs = await import(path.join(routeDir, file.path));
        f = funcs[name] || funcs['default']
      }

      const method = {
        name: name == 'del' ? 'DELETE' : (name || '').toUpperCase(),
        func: f,
        parameters: parameters,
        returnType: func.getReturnType().getText(),
        _file: file
      }
      method.parameters.forEach(p => p._method = method);
      methods.push(method)
    }

    const p = ('/' + file.path)
      .replace(/\[\.\.\]/, '**')
      .replace(/(\[[^\]+]\])/gm, ':$1')
      .replace(/\.[^\.]*/g, '')
      .replace(/\/index$/gi, '/')
      .replace(/\/index\*\*$/gi, '/**')


    return {
      __source__: sourceFile,
      mtimeMs: file.stat.mtimeMs,
      path: p,
      file: file.path,
      methods
    } as ApiifyRouteFile
  }
}
