// @ts-nocheck
import { Apiify } from "open-apiify";


(async () => {
  const app = await Apiify.create('../');

  app.listen();
})().catch(e => { throw e });
