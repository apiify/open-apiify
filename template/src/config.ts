import { Config } from "../.apiify/config";
import { Apiify } from "open-apiify";

const config: Config = Apiify.useConfig();

export default config;
export const env = config.env;
