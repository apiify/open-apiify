// @ts-check

/** @type {() => Promise<import('../.apiify/config').Config>} */
module.exports = async () => {
  return {
    listen: {
      host: '0.0.0.0'
    }
  }
};
