// @ts-check

/** @type {() => Promise<import('../.apiify/config').Config>} */
module.exports = async () => {
  return {
    listen: {
    },
    https: {
      enable: false
    }
  }
};
