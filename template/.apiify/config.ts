import { Options } from "open-apiify/dist/types";
export interface Config extends Options {
  env?: object
};
