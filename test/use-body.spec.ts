import supertest from "supertest";
import Apiify from "../src";
import { IncomingMessage } from "node:http";
import { Stream } from "node:stream";
import PersistentFile from "formidable/PersistentFile";

describe('use-body', () => {
  it('01', (done) => {
    Apiify.create()
      .then(app => {
        app.post('/', async (req, res) => {
          const body = await Apiify.useBody(req);
          expect(body).toEqual({ foo: 'bar' });
          done();
        });

        supertest(app.server)
          .post('/')
          .send({
            foo: 'bar'
          })
          .expect(200)
          .end(() => { });
      });
  });

  it('02 - upload file', (done) => {
    Apiify.create()
      .then(app => {
        app.post('/', async (req, res) => {
          const body = await Apiify.useBody<{ ff1: PersistentFile[] }>(req);
          expect(body.ff1[0].toJSON()).toMatchInlineSnapshot({
            filepath: expect.any(String),
            mimetype: expect.any(String),
            mtime: expect.any(Date),
            newFilename: expect.any(String)
          }, `
{
  "filepath": Any<String>,
  "length": undefined,
  "mimetype": Any<String>,
  "mtime": Any<Date>,
  "newFilename": Any<String>,
  "originalFilename": null,
  "size": 13,
}
`);
          done();
          return '';
        });

        supertest(app.server)
          .post('/')
          .attach('ff1', Buffer.from('hello world 1'))
          .attach('ff2', Buffer.from('hello world 2'))
          // .send({ foo: 'bar' })
          // .expect(200)
          .end(async () => {
          });
      });
  }, 10e3);
});