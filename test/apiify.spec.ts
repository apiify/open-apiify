import Apiify from "../src";
import { ApiifyServer } from "../src/apiify-server";
import agent from "supertest";
import * as zlib from "zlib";

describe('apiify-server', () => {
  it('02', async () => {
    const app = await Apiify.create();
    let res = await agent(app.server)
      .get('/')
    expect(res.status).toBe(404);
  });

  it('03', async () => {
    const app = await Apiify.create();
    app.get('/', (req, res) => {
      return '';
    });

    const res = await agent(app.server)
      .get('/')
    expect(res.status).toBe(200);
  });

  it('04', async () => {
    const app = await Apiify.create();
    app.get('/', (req, res) => {
      return null;
    });

    const res = await agent(app.server)
      .get('/');

    expect(res.status).toBe(404);
  });

  it('05 - response default', async () => {
    const app = await Apiify.create();
    app.get('/', (req, res) => {
    });

    const res = await agent(app.server)
      .get('/');

    expect(res.status).toBe(200);
  });

  it('06 - content-encoding gzip', (done) => {
    Apiify.create()
      .then(async app => {
        app.post('/', async (req, res) => {
          const body = await Apiify.useBody(req)
          expect(body).toBe('test data');
          done();
        });

        let r = agent(app.server)
          .post('/')
          .set('Content-Encoding', 'gzip');
        r.write(
          zlib.gzipSync('test data')
        );
        r.end(() => { });
      });
  }, 10e3);

  it('07 - content-encoding inflate', (done) => {
    Apiify.create()
      .then(async app => {
        app.post('/', async (req, res) => {
          const body = await Apiify.useBody(req)
          expect(body).toBe('test data');
          done();
        });

        let r = agent(app.server)
          .post('/')
          .set('Content-Encoding', 'deflate');
        r.write(
          zlib.deflateSync('test data')
        );
        r.end(() => { });
      });
  }, 10e3);

  it('08 - content-type multipart/form-data', (done) => {
    Apiify.create()
      .then(async app => {
        app.post('/', async (req, res) => {
          try {

            const body = await Apiify.useBody(req);
            expect(body).toMatchInlineSnapshot(`
{
  "foo1": [
    "bar",
  ],
  "foo2": [
    "bar1",
    "bar2",
  ],
}
`);
          } catch (err) {
            console.error(err);
          } finally {
            done();
          }
        });

        agent(app.server)
          .post('/')
          .field('foo1', 'bar')
          .field('foo2', 'bar1')
          .field('foo2', 'bar2')
          .end(() => { });
      });
  });
});


describe('route-file', () => {
  it('01', async () => {
    const app = await Apiify.create();
    let res = await agent(app.server)
      .get('/')
    expect(res.status).toBe(404);
  });
});