import * as path from "node:path";
import tmp from "tmp";
import * as shelljs from "shelljs";
import * as fse from "fs-extra";
import * as _ from "lodash";



const apiifyCliFile = path.resolve(__dirname, "../../src/apiify-cli.ts");
// const apiifyCliFile = path.resolve(__dirname, "../../dist/apiify-cli.js");

const dir = tmp.dirSync()
console.log(dir);
shelljs.cd(dir.name);
shelljs.env.TS_NODE = 'true';
shelljs.exec(`ts-node ${apiifyCliFile} init`);

const pkg = JSON.parse(fse.readFileSync(path.resolve(dir.name, 'package.json'), 'utf-8'));
_.set(pkg, "resolutions.open-apiify", "portal:/C:/workbench/nodejs/open-apiify");
fse.writeFileSync(path.resolve(dir.name, 'package.json'), JSON.stringify(pkg));

shelljs.exec(`yarn install`);
// shelljs.exec(`yarn apiify dev`);
shelljs.exec(`yarn apiify start --build`);



// "resolutions": {
//   "open-apiify": "portal:/C:/workbench/nodejs/open-apiify"
// },