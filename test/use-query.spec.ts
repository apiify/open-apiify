import Apiify from "../src";
import { ApiifyServer } from "../src/apiify-server";
import agent from "supertest";

describe('use-query', () => {
  it('01', (done) => {
    Apiify.create()
      .then(async app => {
        app.get('/', async (req, res) => {
          const query = await Apiify.useQuery(req);
          expect(query).toMatchInlineSnapshot(`
{
  "foo": "bar",
}
`);
          done();
        })

        agent(app.server)
          .get('/')
          .query({ foo: 'bar' })
          .then();
      });
  }, 6e4);
});